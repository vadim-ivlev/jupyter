#!/bin/bash

# docker run --name note -i -t -p 8888:8888 continuumio/anaconda3 /bin/bash -c "/opt/conda/bin/conda install jupyter -y --quiet && mkdir /opt/notebooks && /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='*' --port=8888 --no-browser --allow-root"



# запуск докера jupiter 
echo "***************************"
echo "jupyter/datascience-notebook started"
echo "Open http://localhost:9999/"
echo "***************************"
echo ""
docker run --rm --name science  -p 9999:8888 -v "$(pwd)":/home/jovyan/work jupyter/datascience-notebook /opt/conda/bin/jupyter notebook  --ip="*" --port=8888 --notebook-dir=/home/jovyan/work --no-browser --allow-root  --NotebookApp.token=""

# echo "***************************"
# echo "continuumio/anaconda3 started"
# echo "Open http://localhost:9999/"
# echo "***************************"
# echo ""
# docker run --rm --name science1 -p 9999:8888 -v "$(pwd)":/opt/notebooks continuumio/anaconda3           /opt/conda/bin/jupyter notebook  --ip="*" --port=8888 --notebook-dir=/opt/notebooks    --no-browser --allow-root  --NotebookApp.token=""
