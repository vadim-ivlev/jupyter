FROM continuumio/anaconda3

RUN /opt/conda/bin/conda install jupyter -y --quiet 
RUN mkdir /opt/notebooks

# Generate jupyter_notebook_config.py file
RUN jupyter notebook --generate-config

# set password 'mob'
# Read https://jupyter-notebook.readthedocs.io/en/stable/public_server.html
RUN echo "c.NotebookApp.password = 'sha1:1f996cae445b:9894e7a04ac8ce86ea491c6e9a8640fb3ad8ab36'" >> /root/.jupyter/jupyter_notebook_config.py


CMD /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='*' --port=8888 --no-browser --allow-root

